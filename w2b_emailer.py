#!/usr/bin/env python

"""
This is a program listening on a configured email server for new messages
with 'wiki2beamer' content which gets converted into a pdf including the
presentation which is sent back by email to the original sender. If there is
an error while generating the presentation document, an error email message is
sent back instead of the document.
The subject line of the email should start with "!w2b". Any additional text in
the subject line is ignored in the current implementation.
The body should contain the wiki2beamer formatted text.


 - TODO Attached pictures in the original email message should be saved to the
   temporary directory and - if properly referenced in the wiki2beamer source
   - be included in the presentation document (automatically)


references:
 - wiki2beamer: http://wiki2beamer.sourceforge.net/
"""

import imaplib
import imapidle  # used for overriding imaplib idle connection
import logging
import email
import email.utils
import os
import smtplib
from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart
from email.mime.application import MIMEApplication
import sys
import subprocess
import tempfile
import ConfigParser


savepath = os.path.expanduser('~') + '/.w2b_emailerrc'


class W2bEmailer():
    """
     - Connect to IMAP mail account
     - wait for incoming messages with special subject '!w2b'
     - save this message
     - store the body of the mail in a temporary folder as wiki2beamer-source
     - Process this file with wiki2beamer
     - Process the output of w2b with pdflatex
     - Send the output pdf back to original sender
    """
    def __init__(self, options=None):
        if options is not None:
            self.options = options
            self.simulate = options.simulate
        else:
            self.simulate = False
            self.file = None

        store = ConfigParser.SafeConfigParser()
        store.read(savepath)
        try:
            self.imaphost = store.get('imap', 'hostname')
            self.imapuser = store.get('imap', 'username')
            self.imappass = store.get('imap', 'password')
        except ConfigParser.NoSectionError:
            print("""
You are missing the mandatory configuration file %s with the proper format.
Example:
[imap]
hostname = imap.myhost.com
username = me
password = secret
""" % savepath)
            sys.exit(1)

        self.header = """
Dear wiki2beamer2latex2pdf2email user,
"""
        self.footer = """
Sincerely yours,
wiki2beamer emailer
"""

    def check_imap_email_with_idle(self):
        self.conn = imaplib.IMAP4_SSL(self.imaphost)
        self.conn.login(self.imapuser, self.imappass)
        self.conn.select(readonly=True)
        conn = self.conn
        loop = True
        logging.info("Idling for email updates")
        while loop:
            for uid, status in self.conn.idle():
                if status == "EXISTS":
                    conn.done()
                    status, data = conn.fetch(uid, '(RFC822)')
                    logging.debug("data: %s" % data)
                    msg = email.message_from_string(data[0][1])
                    if "!w2b" in msg['Subject']:
                        logging.debug("Received w2b email: %s" % msg)
                        self.process_msg(msg)

    def process_msg(self, msg):
        self.save_message(msg)
        self.process_file_with_wiki2beamer()
        self.send_pdf_back()
        
    def get_w2b_content(self, msg):
        return msg.get_payload(decode=True)

    def save_message(self, msg, doc_name='w2b_emailer.w2b'):
        """
        save wiki2beamer content to a temporary file

        store the body of the mail in a temporary folder as wiki2beamer-source
        """
        self.receiver = msg['From']
        w2b_content = self.get_w2b_content(msg)
        logging.debug("wiki2beamer content: %s" % w2b_content)
        tempdir = tempfile.mkdtemp(suffix='w2b_email')
        self.tmpfilename = os.path.join(tempdir, doc_name)
        open(self.tmpfilename, 'wb').write(w2b_content)

    def process_file_with_wiki2beamer(self, filename=None):
        if filename is None:
            filename = self.tmpfilename
        name_without_ext = os.path.splitext(filename)[0]
        outname = name_without_ext + '.tex'
        logging.debug("Calling wiki2beamer -o %s %s" % (outname, filename))
        try:
            subprocess.check_call(['wiki2beamer', '-o', outname, filename])
        except subprocess.CalledProcessError as e:
            logging.warn("subprocess error on wiki2beamer: %s" % e)
            if self.options.file is not None: raise(e)
            msg = MIMEMultipart()
            text = MIMEText(self.header
                    + '\n'
                    + """
Unfortunately your presentation could not be processed from the provided
content into LaTeX.
Error message:
%s
""" % (e)
                    + '\n'
                    + self.footer)
            msg.attach(text)
            self.send_email(subject_add=' - failed: LaTeX source could not be generated', msg=msg)
            raise(e)

        # Just to be sure for latex, needs check if needed or if latex behaves sane and only writes to tmpdir
        output_dir = os.path.abspath(os.path.dirname(outname))
        try:
            subprocess.check_call(['pdflatex', '-halt-on-error', '--interaction=batchmode', '-output-directory=%s' % output_dir, outname])
        except subprocess.CalledProcessError as e:
            logging.warn("subprocess error on pdflatex: %s" % e)
            if self.options.file is not None: raise(e)
            logfile = name_without_ext + '.log'
            msg = MIMEMultipart()
            text = MIMEText(self.header
                    + '\n'
                    + """
Unfortunately your presentation could not be processed from the generated
LaTeX source into a PDF file. The last 20 lines from output are:
{{{
%s
}}}

The full log file is attached as %s.
""" % (''.join(open(logfile).readlines()[-20:]), logfile)
                    + '\n'
                    + self.footer)
            msg.attach(text)
            logfile_attachment = MIMEText(open(logfile, 'rb').read())
            logfile_basename = os.path.basename(logfile)
            logfile_attachment.add_header('Content-Disposition', 'attachment', filename=logfile_basename)
            msg.attach(logfile_attachment)
            self.send_email(subject_add=' - failed: logfile %s attached' % logfile_basename, msg=msg)
            raise(e)

    def send_email(self, msg, subject_add='', smtp_host='localhost'):
        sender = 'wiki2beamer emailer <krz@iis.fhg.de>'
        msg['Subject'] = 'Re: !wb: your processed presentation' + subject_add
        msg['From'] = sender
        msg['To'] = self.receiver
        logging.info("send email to " + self.receiver + " from " + sender + " with subject %s\n" % msg['Subject'])
        if self.simulate:
            logging.info("Only simulation, no email sent.")
        else:
            s = smtplib.SMTP(smtp_host)
            s.sendmail(sender, self.receiver, msg.as_string())

    def send_pdf_back(self, pdf_filename=None):
        """Send the output pdf back to original sender"""
        msg = MIMEMultipart()
        text = MIMEText(self.header
            + '\n'
            + 'Please find attached the PDF file for the content you generated.'
            + '\n'
            + self.footer)
        msg.attach(text)
        if pdf_filename is None:
            pdf_filename = os.path.splitext(self.tmpfilename)[0] + '.pdf'
        pdf = MIMEApplication(open(pdf_filename).read(), _subtype='pdf')
        pdf_basename = os.path.basename(pdf_filename)
        pdf.add_header('Content-Disposition', 'attachment', filename=pdf_basename)
        msg.attach(pdf)
        self.send_email(subject_add=' - success: %s attached' % pdf_basename, msg=msg)

    def test(self, filename='w2b_emailer_test_msg.pickle'):
        if os.path.exists(filename):
            import pickle
            msg = pickle.load(open(filename, 'rb'))
            self.process_msg(msg)


if __name__ == "__main__":
    from optparse import OptionParser
    parser = OptionParser()
    parser.set_description(__doc__)

    parser.add_option("-?", action="store_true",
        help="Show this help", dest="help")
    parser.add_option("-v", "--verbose",
        help="Increase verbosity level, specify multiple times to increase verbosity",
        action="count", dest="verbose", default=1)
    parser.add_option("-f", "--file",
        help="File-Mode, Directly call wiki2beamer on a file supplied as argument, not listening on emails")
    parser.add_option("-s", "--simulate",
        help="Only simulate email sending", action="store_true")
    parser.add_option("-t", "--test",
        help="Test using a supplied test message file instead of waiting for emails, default 'w2b_emailer_test_msg.pickle'")
    parser.add_option("-e", "--stop_on_error",
        help="Stop on first error, otherwise runs in a loop (Only for IMAP idle mode)",
        action="store_true", default=False)

    (options, args) = parser.parse_args()
    if options.help:
        parser.print_help()
        sys.exit(0)
    verbose_to_log = {
        0: logging.CRITICAL,
        1: logging.ERROR,
        2: logging.WARN,
        3: logging.INFO,
        4: logging.DEBUG
    }
    if options.verbose > 4: level = logging.DEBUG
    else: level = verbose_to_log[options.verbose]
    logging.basicConfig(format='#%(levelname)s:%(message)s', level=level)
    if options.test:
        W2bEmailer(options).test(options.test)
    elif options.file:
        w2b = W2bEmailer(options)
        w2b.process_file_with_wiki2beamer(options.file)
    else:
        if options.stop_on_error:
            W2bEmailer(options).check_imap_email_with_idle()
        else:
            while True:
                try:
                    W2bEmailer(options).check_imap_email_with_idle()
                except subprocess.CalledProcessError as e:
                    pass
